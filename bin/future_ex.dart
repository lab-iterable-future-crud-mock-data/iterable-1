Future<String> createOrderMes() async {
  var order = await fetchUserOrder();
  return 'Your order is $order';
}

Future<String> fetchUserOrder() =>
    Future.delayed(const Duration(seconds: 0), () => 'Large Latte');
void main() async {
  print('Fetching user oder . . . ');
  print(await createOrderMes());
  print('Finish');
}
