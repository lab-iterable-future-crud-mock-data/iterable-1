Future<String> fetchUserOrder() {
  return Future.delayed(const Duration(seconds: 4), () => 'Coaco');
}

Future<void> printOrderMes() async {
  print('Waiting user order . . . ');
  var order = await fetchUserOrder();
  print('Your order is : $order');
}

void main() async {
  countSec(4);
  await printOrderMes();
}

void countSec(int s) {
  for (var i = 1; i <= s; i++) {
    Future.delayed(Duration(seconds: i), () => print(i));
  }
}
