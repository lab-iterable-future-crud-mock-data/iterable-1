import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  const foods = ['Salad', 'Popcorn', 'Toast', 'Langa', 'Mamaa'];
  Iterable<String> iterableFoods = foods;
  for (var food in foods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('Last element is ${foods.last}');

  var foundItem1 = foods.firstWhere((element) => element.length > 5);
  print(foundItem1);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem2 = foods.firstWhere(predicate);
  print(foundItem2);

  var foundItem3 =
      foods.firstWhere((item) => item.length > 10, orElse: () => 'None!');
  print(foundItem3);

  var foundItem4 = foods.firstWhere(
      (item) => item.startsWith('M') && item.contains('a'),
      orElse: () => 'None!');
  print(foundItem4);

  if (foods.any((item) => item.contains('a'))) {
    print('At least on item contains " a " ');
  }
  if (foods.every((item) => item.length >= 5)) {
    print('All item have lenth >=5');
  }
  var numbers = const [1, -2, 3, 42, 0, 4, 5, 6];
  var evenNumbers = numbers.where((number) => number.isEven);
  for (var number in evenNumbers) {
    print('$number is even.');
  }
  if (evenNumbers.any((number) => number.isNegative)) {
    print('evenNumbers contains neg numbers.');
  }
  var largeNumber = evenNumbers.where((number) => number > 1000);
  if (largeNumber.isEmpty) {
    print('Null');
  }
  var numbersUntilZero = numbers.takeWhile((number) => number != 0);
  print('Number until 0: $numbersUntilZero');
  var numbersStartZero = numbers.skipWhile((number) => number != 0);
  print('Number start 0: $numbersStartZero');

  var numbersByTwo = const [1, -2, 3, 42].map((number) => number * 2);
  print('Number * 2: $numbersByTwo');
}
