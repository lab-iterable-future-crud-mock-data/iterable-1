bool anyUserUnder18(Iterable<User> users) {
  return users.any((user) => user.age < 18);
}

bool everyUserOver13(Iterable<User> users) {
  return users.every((user) => user.age > 13);
}

Iterable<User> filterOutUnder21(Iterable<User> users) {
  return users.where((user) => user.age >= 21);
}

Iterable<User> findShortName(Iterable<User> users) {
  return users.where((user) => user.name.length <= 4);
}

Iterable<String> getNameAndAge(Iterable<User> users) {
  return users.map((user) => '${user.name} is ${user.age}');
}

void main() {
  var users = [
    User(name: 'Kawaii', age: 14),
    User(name: 'Kawagi', age: 15),
    User(name: 'Kawashi', age: 16),
    User(name: 'Kawaji', age: 21),
    User(name: 'Kami', age: 25)
  ];
  if (anyUserUnder18(users)) {
    print('Have user under 18');
  }
  if (everyUserOver13(users)) {
    print('Have user are over 13');
  }

  var ageMoreThan21 = filterOutUnder21(users);
  for (var user in ageMoreThan21) {
    print(user.toString());
  }
  var shortName = findShortName(users);
  for (var user in shortName) {
    print(user.toString());
  }
  var nameAndAges = getNameAndAge(users);
  for (var user in nameAndAges) {
    print(user);
  }
}

class User {
  String name;
  int age;
  User({required this.name, required this.age});
  String toString() {
    return '$name,$age';
  }
}
